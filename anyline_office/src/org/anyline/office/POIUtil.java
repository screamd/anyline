package org.anyline.office;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.anyline.entity.DataRow;
import org.anyline.entity.DataSet;
import org.anyline.office.docx.tag.Each;
import org.anyline.office.docx.tag.Tag;
import org.anyline.util.ImgUtil;
import org.anyline.util.regular.RegularUtil;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

public class POIUtil {

    /**
     * 根据模板生成新word文档
     * 判断表格是需要替换还是需要插入，判断逻辑有$为替换，表格无$为插入
     * @param inputUrl 模板存放地址
     * @param outPutUrl 新文档存放地址
     * @param textMap 需要替换的信息集合
     * @param tableList 需要插入的表格信息集合
     * @return 成功返回true,失败返回false
     */
    public static boolean changWord(String inputUrl, String outputUrl,
            Map<String, String> global, List<Map<String,String>> p1) {

        //模板转换默认成功
        boolean changeFlag = true;
        try {
            //获取docx解析对象
            XWPFDocument doc = new XWPFDocument(POIXMLDocument.openPackage(inputUrl));
            //解析替换文本段落对象
            changeText(doc, global);
            //解析替换表格对象
           // changeTable(document, textMap, tableList);

            XWPFParagraph p = getParagraph(doc, "{{pos_p1}}");
            XWPFParagraph p1_title = doc.insertNewParagraph(p.getCTP().newCursor());
            copyParagraph(p1_title, p);
            //生成新的word
            File file = new File(outputUrl);
            FileOutputStream stream = new FileOutputStream(file);
            doc.write(stream);
            stream.close();
            
        } catch (IOException e) {
            e.printStackTrace();
            changeFlag = false;
        }

        return changeFlag;

    }
    private static void CopyRun(XWPFRun target, XWPFRun source) {
        target.getCTR().setRPr(source.getCTR().getRPr());
        // 设置文本
        target.setText(source.text());
    }

    private static void copyParagraph(XWPFParagraph target, XWPFParagraph source) {
        // 设置段落样式
        target.getCTP().setPPr(source.getCTP().getPPr());
        // 添加Run标签
        for (int pos = 0; pos < target.getRuns().size(); pos++) {
            target.removeRun(pos);
        }
        for (XWPFRun s : source.getRuns()) {
            XWPFRun targetrun = target.createRun();
            CopyRun(targetrun, s);
        }
    }
    /**
     * 替换段落文本
     * @param document docx解析对象
     * @param textMap 需要替换的信息集合
     */
    public static void changeText(XWPFDocument doc, Map<String, String> textMap){
        //获取段落集合
        List<XWPFParagraph> paragraphs = doc.getParagraphs();
        for (XWPFParagraph p : paragraphs) {
            //判断此段落时候需要进行替换
            String text = p.getText();
            if(checkText(text)){
                List<XWPFRun> runs = p.getRuns();
                for (XWPFRun run : runs) {
                    //替换模板原来位置
                    run.setText(changeValue(run.toString(), textMap),0);
                }
            }
        }
    }

    /**
     * 替换表格对象方法
     * @param document docx解析对象
     * @param textMap 需要替换的信息集合
     * @param tableList 需要插入的表格信息集合
     */
    public static void changeTable(XWPFDocument document, Map<String, String> textMap,
            List<String[]> tableList){
        //获取表格对象集合
        List<XWPFTable> tables = document.getTables();
        for (int i = 0; i < tables.size(); i++) {
            //只处理行数大于等于2的表格，且不循环表头
            XWPFTable table = tables.get(i);
            if(table.getRows().size()>1){
                //判断表格是需要替换还是需要插入，判断逻辑有$为替换，表格无$为插入
                if(checkText(table.getText())){
                    List<XWPFTableRow> rows = table.getRows();
                    //遍历表格,并替换模板
                    eachTable(rows, textMap);
                }else{
//                  System.out.println("插入"+table.getText());
                    insertTable(table, tableList);
                }
            }
        }
    }





    /**
     * 遍历表格
     * @param rows 表格行对象
     * @param textMap 需要替换的信息集合
     */
    public static void eachTable(List<XWPFTableRow> rows ,Map<String, String> textMap){
        for (XWPFTableRow row : rows) {
            List<XWPFTableCell> cells = row.getTableCells();
            for (XWPFTableCell cell : cells) {
                //判断单元格是否需要替换
                if(checkText(cell.getText())){
                    List<XWPFParagraph> paragraphs = cell.getParagraphs();
                    for (XWPFParagraph paragraph : paragraphs) {
                        List<XWPFRun> runs = paragraph.getRuns();
                        for (XWPFRun run : runs) {
                            run.setText(changeValue(run.toString(), textMap),0);
                        }
                    }
                }
            }
        }
    }

    /**
     * 为表格插入数据，行数不够添加新行
     * @param table 需要插入数据的表格
     * @param tableList 插入数据集合
     */
    public static void insertTable(XWPFTable table, List<String[]> tableList){
        //创建行,根据需要插入的数据添加新行，不处理表头
        for(int i = 1; i < tableList.size(); i++){
            XWPFTableRow row =table.createRow();
        }
        //遍历表格插入数据
        List<XWPFTableRow> rows = table.getRows();
        for(int i = 1; i < rows.size(); i++){
            XWPFTableRow newRow = table.getRow(i);
            List<XWPFTableCell> cells = newRow.getTableCells();
            for(int j = 0; j < cells.size(); j++){
                XWPFTableCell cell = cells.get(j);
                cell.setText(tableList.get(i-1)[j]);
            }
        }

    }



    /**
     * 判断文本中时候包含$
     * @param text 文本
     * @return 包含返回true,不包含返回false
     */
    public static boolean checkText(String text){
        boolean check  =  false;
        if(text.indexOf("{")!= -1){
            check = true;
        }
        return check;

    }

    /**
     * 匹配传入信息集合与模板
     * @param value 模板需要替换的区域
     * @param textMap 传入信息集合
     * @return 模板需要替换区域信息集合对应值
     */
    public static String changeValue(String value, Map<String, String> textMap){
        Set<Entry<String, String>> textSets = textMap.entrySet();
        for (Entry<String, String> textSet : textSets) {
            //匹配模板与替换值 格式${key}
            String key = "{"+textSet.getKey()+"}";
            if(value.indexOf(key)!= -1){
                value = textSet.getValue();
            }
        }
        return value;
    }


    /**
     * 根据关键词查找段落
     * @param doc
     * @param key
     * @return
     */
    public static XWPFParagraph getParagraph(XWPFDocument doc, String key){
    	List<XWPFParagraph> paragraphs = doc.getParagraphs();
        for (XWPFParagraph p : paragraphs) {
            String text = p.getText();
            if(text.contains(key)){
            	return p;
            }
        }
    	return null;
    }
    public static List<Tag> parseTags(List<XWPFParagraph> ps){
    	List<Tag> tags = new ArrayList<Tag>();
		int size = ps.size();
    	for(int i=0; i<size; i++){
    		Tag tag = parseTag(ps, i);
    		if(!tag.isEmpty()){
    			tags.add(tag);
    			i += tag.getRows()+2; 
    		}else{
    			break;
    		}
    	}
    	return tags;
    }

    public static void removeTags(XWPFDocument doc, List<XWPFParagraph> ps){
    	for(XWPFParagraph p:ps){
    		List<XWPFRun> runs = p.getRuns();
    		for(XWPFRun run:runs){
    		}
    	}
    }
    public static Tag parseTag(List<XWPFParagraph> ps, int begin){
    	Tag tag = null;
    	int size = ps.size();
    	for(int i=begin; i<size; i++){
    		XWPFParagraph p = ps.get(i);
    		String txt = p.getText().trim();
    		if(txt.startsWith("<al")){
    			if(txt.startsWith("<al:each")){
    				tag = new Each();
    				if(i>begin){
    					//子each
    					Tag item = parseTag(ps, i);
    					tag.addItem(item);
    					i += item.getRows()-1;
    				}else{
    					tag.setBeginTag(p);
    				}
    			}
    		}else if(txt.startsWith("</al:each>")){
    			tag.setEndTag(p);
    			break;
    		}else{
    			tag.addItem(p);
    		}
    	}
    	return tag;
    }
    public static void exeTag(XWPFDocument doc, List<Tag> tags, Map<String,Object> values){
    	for(Tag tag:tags){
    		exeTag(doc, tag, values);
    	}
    }
    public static void exeTag(XWPFDocument doc, Tag tag, Map<String,Object> values){
    	if(tag instanceof Each){
    		exeTag(doc, (Each)tag, values);
    	}
    }
    /**
     * 
     * @param doc
     * @param tag
     * @param values 全局values
     */
    public static void exeTag(XWPFDocument doc, Each tag, Map<String,Object> values){
    	String itemsKey = tag.getItemsKey();
    	if(null == itemsKey){
    		return;
    	}
    	if(itemsKey.startsWith("${")){
    		itemsKey = itemsKey.replace("${", "").replace("}", "");
    	}
    	//each values
    	Object tmp = values.get(itemsKey);
    	Collection<Map<String,Object>> eachValues = (Collection<Map<String,Object>>)tmp;
    	if(null == eachValues){
    		return;
    	}
    	for(Map<String,Object> itemValues:eachValues){//each
    		values.put(tag.getVarKey(), itemValues);
    		//复制标签体
    		List<Object> ps = tag.getItems();
    		for(Object p:ps){//each body
    			if(p instanceof XWPFParagraph){
    				XWPFParagraph tar = doc.insertNewParagraph(tag.getBeginTag().getCTP().newCursor());
    				copyParagraph(tar, (XWPFParagraph)p);
    				setIntimeValue(tar, values);
    			}else if(p instanceof Tag){
    				//子标签 未实现
    				exeTag(doc, (Tag)p, values);
    			}
    		}
    	}
    }
    /**
     * 设置段落值
     * @param p
     * @param values
     */
    public static void setIntimeValue(XWPFParagraph p, Map<String,Object> values){
    	List<XWPFRun> runs = p.getRuns();
    	for(XWPFRun run:runs){
    		String txt = run.text();
    		txt = parseIntimeValue(txt, values);
    		run.setText(txt,0);
    	}
    }
    public static String parseIntimeValue(String src, Map<String,Object> values){
    	List<String> keys = RegularUtil.cuts(src,"${", "}");
    	for(String key:keys){
    		Object value = getValue(key, values);
    		if(null == value){
    			src = src.replace("${"+key+"}", "");
    		}else{
    			src = src.replace("${"+key+"}", value.toString());
    		}
    		
    	}
    	return src;
    }
    public static Object getValue(String key, Map<String,Object> values){
    	Object value = null;
    	if(key.contains(".")){
    		String k0 = key.substring(0, key.indexOf("."));
    		String k1 = key.substring(key.indexOf(".")+1);
    		Object tmp = values.get(k0);
    		if(tmp instanceof Map){
    			value = getValue(k1, (Map<String,Object>)tmp);
    		}
    	}else{
    		value = values.get(key);
    	}
    	return value;
    }
    
    public static boolean test(String inputUrl, String outputUrl) {
        //模板转换默认成功
        boolean changeFlag = true;
        try {
            //获取docx解析对象
            XWPFDocument doc = new XWPFDocument(POIXMLDocument.openPackage(inputUrl));
            List<XWPFParagraph> ps = doc.getParagraphs();
            List<Tag> tags = parseTags(ps);
            Map<String,Object> values = new HashMap<String,Object>();
            DataSet set = new DataSet();
            for(int i=0; i<3; i++){
            	DataRow row = new DataRow();
            	row.put("dept_nm", "部<br>门"+i);
            	set.add(row);
            }
            values.put("set", set);
            exeTag(doc, tags, values);
            File file = new File(outputUrl);
            FileOutputStream stream = new FileOutputStream(file);
            doc.write(stream);
            stream.close();
            
        } catch (IOException e) {
            e.printStackTrace();
            changeFlag = false;
        }

        return changeFlag;

    }
    public static void main(String[] args) {
        //模板文件地址
        String inputUrl = "D:\\temp.docx";
        //新生产的模板文件
        String outputUrl = "D:\\out.docx";

        Map<String, String> global 	= new HashMap<String, String>();	//全局变量
        List<Map<String,String>> p1 = new ArrayList<Map<String,String>>();	//基础设施和团队建设进展
        for(int i=0; i<3; i++){
        	Map<String,String> map = new HashMap<String,String>();
        	map.put("dept_nm", "部门"+i);
        	map.put("p11", "入驻和实验条件改造工作具体内容");
        	map.put("p12", "团队建设工作具体内容");
        	map.put("p13", "管理制度建设工作具体内容");
        	p1.add(map);
        }
        //p11_title:1.入驻和实验条件改造工作
        //p12_title:2.团队建设工作
        //p13_title:3.管理制度建设工作
        global.put("y", "2018");
        global.put("m", "06");
        global.put("d", "10");
        global.put("i", "2");
        global.put("j", "10");
        global.put("w", "3-6");
        

        List<String[]> testList = new ArrayList<String[]>();
        testList.add(new String[]{"1","1AA","1BB","1CC"});
        testList.add(new String[]{"2","2AA","2BB","2CC"});
        testList.add(new String[]{"3","3AA","3BB","3CC"});
        testList.add(new String[]{"4","4AA","4BB","4CC"});

        //changWord(inputUrl, outputUrl, global, p1);
        //test(inputUrl, outputUrl);
//        String html = FileUtil.readFile(new File("D:\\a.html"),"UTF-8").toString();
//        html = RegularUtil.removeHtmlTagExcept(html, "img","p","br").replace("\r\n", "");
//        List<String> ps = RegularUtil.cuts(html, "<p",">","</p");
//        for(String p:ps){
//        	System.out.println(p);
//        }
        String txt = "<a href='www.baidu.com'>link</a>";
        String result = RegularUtil.cut(txt, "<a.*>","</a>");
        System.out.println(result);
    }
}
