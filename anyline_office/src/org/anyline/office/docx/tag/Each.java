package org.anyline.office.docx.tag;

import org.anyline.util.BasicUtil;
import org.anyline.util.regular.RegularUtil;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;


public class Each extends Tag{
	private String varKey;
	private String itemsKey;
	private String beginKey;
	private String endKey;
	private int begin;
	private int end = -1;
	public String getVarKey() {
		return varKey;
	}
	public void setVarKey(String varKey) {
		this.varKey = varKey;
	}
	public String getItemsKey() {
		return itemsKey;
	}
	public void setItemsKey(String itemsKey) {
		this.itemsKey = itemsKey;
	}
	public String getBeginKey() {
		return beginKey;
	}
	public void setBeginKey(String beginKey) {
		this.beginKey = beginKey;
	}
	public String getEndKey() {
		return endKey;
	}
	public void setEndKey(String endKey) {
		this.endKey = endKey;
	}

	public int getBegin() {
		return begin;
	}
	public void setBegin(int begin) {
		this.begin = begin;
	}
	public int getEnd() {
		return end;
	}
	public void setEnd(int end) {
		this.end = end;
	}
	public String toString(){
		StringBuilder builder = new StringBuilder();
		builder.append(beginTag.getText()).append("\n");
		for(Object item:items){
			if(item instanceof Each){
				builder.append(item.toString()).append("\n");
			}else if(item instanceof XWPFParagraph){
				builder.append(((XWPFParagraph)item).getText()).append("\n");
			}
		}
		builder.append(endTag.getText());
		return builder.toString();
	}

	public void setBeginTag(XWPFParagraph beginTag) {
		super.setBeginTag(beginTag);
		String txt = beginTag.getText();
		String var = RegularUtil.cut(txt, "var","=","\"","\"");
		this.setVarKey(var);
		String items = RegularUtil.cut(txt, "items","=","\"","\"");
		this.setItemsKey(items);
		String begin = RegularUtil.cut(txt, "begin","=","\"","\"");
		if(BasicUtil.isNotEmpty(begin)){
			if(begin.startsWith("${")){
				this.setBeginKey(begin);
			}else{
				this.setBegin(BasicUtil.parseInt(begin, 0));
			}
		}
		String end = RegularUtil.cut(txt, "end","=","\"","\"");
		if(BasicUtil.isNotEmpty(end)){
			if(end.startsWith("${")){
				this.setEndKey(end);
			}else{
				this.setEnd(BasicUtil.parseInt(end, 0));
			}
		}
		
	}
	
}
