<%@ page language="java" isELIgnored="false" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.anyline.org/core" prefix="al"%>
<%@ taglib uri="http://www.anyline.org/weixin" prefix="wx"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String curType = org.anyline.util.WebUtil.clientType(request);
if("weixin".equals(curType)){
%>
	<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
	<wx:config apis="openLocation,getLocation,scanQRCode" debug="false"></wx:config>
<%} %>

<div id="map_container" style="display:none;"></div>
<script type="text/javascript">
var login_back_url = '${LOGIN_BACK_URL}';
al.clientType = '<%=curType%>';
al.isApp = function(){
	if(localStorage.getItem("anyline_is_app")*1 == 1){
		return true;
	}
	if(typeof window.twoant == 'object'){
		return true;
	}
	if(typeof twoant_copy == 'function'){
		return true;
	}
	if(al.clientType == 'app'){
		return true;
	}
	return false;
}
if(al.isApp()){
	localStorage.setItem("anyline_is_app", 1);
}

//定位callback(token, result, lon, lat);
function cfGetLocation(token,callback){

	if(al.isApp()){
		cfGetLocation_APP(token,al.getFnName(callback));
	}else if(al.isWeixin()){
		wx.ready(function(){
			cfGetLocation_WX(token,callback);
		});
	}else{
		cfGetLocation_GD(token,callback);
	}
}

function cfGetLocation_APP(token,callback){
	app.getLocation(token, callback);
}
function cfGetLocation_WX(token,callback){
	wx.getLocation({
	    type: 'wgs84', // 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
	    success: function (res) {
	        var lat = res.latitude; // 纬度，浮点数，范围为90 ~ -90
	        var lon = res.longitude; // 经度，浮点数，范围为180 ~ -180。
	        var speed = res.speed; // 速度，以米/每秒计
	        var accuracy = res.accuracy; // 位置精度
	        callback(token, true, lon, lat);
	    }
	});
}
function cfGetLocation_GD(token,callback){
	console.log('get location gaode');
	 	var map, geolocation;
	    map = new AMap.Map('map_container');
	    map.plugin('AMap.Geolocation', function() {
	        geolocation = new AMap.Geolocation({
	            enableHighAccuracy: true,//是否使用高精度定位，默认:true
	            timeout: 10000,          //超过10秒后停止定位，默认：无穷大
	            buttonOffset: new AMap.Pixel(10, 20),//定位按钮与设置的停靠位置的偏移量，默认：Pixel(10, 20)
	            zoomToAccuracy: true,      //定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
	            buttonPosition:'RB'
	        });
	        geolocation.getCurrentPosition();
	        AMap.event.addListener(geolocation, 'complete', onComplete);//返回定位信息
	        AMap.event.addListener(geolocation, 'error', onError);      //返回定位出错信息
	    });
	    //解析定位结果
	    function onComplete(data) {
	    	callback(token, true, data.position.getLng(), data.position.getLat());
	    }
	    //解析定位错误信息
	    function onError(data) {
	    	callback(token, false, -1, -1);
	    }
}

//微信APP登录
function fnLogin_WX_APP(){
	var state = 'back:${LOGIN_BACK_URL},action:log';
	app.loginWX('tk',state,'fnLogin_WX_APP_callback');
}
function fnLogin_WX_APP_callback(token, result, code, state){
	if(result){
		al.tips('登录成功');
		location.href = '/wap/hm/lg/cb_wx_app?code='+code+'&state='+state;
	}else{
		al.tips('登录取消');
	}
}

//QQ APP登录
function fnLogin_QQ_APP(){
	var state = 'back:${LOGIN_BACK_URL},action:log';
	app.loginQQ('tk',state,'fnLogin_QQ_APP_callback');
}
function fnLogin_QQ_APP_callback(token, result, code, state){
	if(result){
		al.tips('登录成功');
		location.href = '/wap/hm/lg/cb_qq_app?code='+code+'&state='+state;
	}else{
		al.tips('登录取消');
	}
}

//扫码
function cfScanCode(callback){
	if(al.isWeixin()){
		try{
		wx.scanQRCode({
		    needResult: 1, // 默认为0，扫描结果由微信处理，1则直接返回扫描结果，
		    //scanType: ["qrCode","barCode"], // 可以指定扫二维码还是一维码，默认二者都有
		    success: function (res) {
		    	var result = res.resultStr; // 当needResult 为 1 时，扫码返回的结果
		    	var strs= new Array(); 
		    	strs=result.split(",");
		    	var code = '';
		    	var type = '';
		    	if(strs.length == 1){
		    		code = strs[0];
		    	}else if(strs.length > 1){
		    		type = strs[0];
		    		code = strs[1];
		    	}
		    	callback('',true,code);
			}
		});
		}catch(e){
		}
	}else{
		app.scanCode('tk', al.getFnName(callback));
	}
}
//扫码
function fnScanCode_test(){
	cfScanCode(function(code, type){
		if(code.indexOf('BK_PARSE') ==0){
			al.ajax({
				url:'/js/hm/code/parse',
				data:{code:code},
				callback:function(result, data, msg){
					if(result){
						var url = data['URL']; 
						if(url){
							location.href = url;
						}
					}else{
						al.tips(msg);
					}
				}
			});	
		}
	});
}
</script>
<c:if test="${empty SERVER_SESSION_CUR_MEMBER}">
</c:if>

<script>
var app = {};
//退出应用
app.exit = function(token){
	if(!al.isApp()){
		return false;
	}
	if(al.isIOS()){
		return ios.exit(token);
	}else{
		return android.exit(token);
	}
}
app.copy = function(token, txt){
	if(!al.isApp()){
		return false;
	}
	if(al.isIOS()){
		return ios.copy(token,txt);
	}else{
		return android.copy(token);
	}
}
//定位callback(token, result, lon, lat)
app.getLocation = function(token,callback){
	if(!al.isApp()){
		return false;
	}
	if(al.isIOS()){
		return ios.getLocation(token,callback);
	}else{
		return android.getLocation(token,callback);
	}
}
//微信登录callback(token, result, code)
app.loginWX = function(token,state,callback){
	if(!al.isApp()){
		return false;
	}
	if(al.isIOS()){
		return ios.loginWX(token,state,callback);
	}else{
		return android.loginWX(token,state,callback);
	}
}

//分享到微信 type(1:好友) callback(tkoken, result)
app.shareWX = function(token, type, url, img, title, msg, callback){
	if(!al.isApp()){
		return false;
	}
	if(al.isIOS()){
		return ios.shareWX(token, type, url, img, title, msg, callback);
	}else{
		return android.shareWX(token, type, url, img, title, msg, callback);
	}
}

//QQ登录callback(token, result, code)
app.loginQQ = function(token,state,callback){
	if(!al.isApp()){
		return false;
	}
	if(al.isIOS()){
		return ios.loginQQ(token,state,callback);
	}else{
		return android.loginQQ(token,state,callback);
	}
}
//分享到QQ type(1:好友) callback(tkoken, result)
app.shareQQ = function(token, type, url, img, title, msg, callback){
	if(!al.isApp()){
		return false;
	}
	if(al.isIOS()){
		return ios.shareQQ(token, type, url, img, title, msg, callback);
	}else{
		return android.shareQQ(token, type, url, img, title, msg, callback);
	}
}
//扫码callback(token, result, code);
app.scanCode = function(token, callback){
	if(!al.isApp()){
		return false;
	}
	if(al.isWeixin()){
		
	}else if(al.isIOS()){
		return ios.scanCode(token, callback);
	}else{
		return android.scanCode(token, callback);
	}
}
//支付calback(token, result, order)
app.pay = function(token, order, tool, callback){
	if(!al.isApp()){
		return false;
	}
	if(!tool){
		return false;
	}
	if(al.isIOS()){
		return ios.pay(token, order, tool, callback);
	}else{
		return android.pay(token, order, tool, callback);
	}
}

//登录成功后注册推送
app.jpushListenerReg = function(token, code, method){
	if(!al.isApp()){
		return false;
	}

	if(al.isIOS()){
		return ios.jpushListenerReg(token, code, method);
	}else{
		return android.jpushListenerReg(token, code, method);
	}
}
app.readPush = function(qty){
	if(!al.isApp()){
		return false;
	}
	if(al.isIOS()){
		return ios.twoant_readPush(qty);
	}else{
		return android.twoant_readPush(qty);
	}
}
var ios = {};
//退出应用
ios.exit = function(token){
	try{
		twoant_exit(token);
	}catch(e){
		al.tips("退出应用异常："+e);
	}
}
ios.copy = function(token,txt){
	try{
		twoant_copy(token, txt);
	}catch(e){
		al.tips("复制："+e);
	}
}
//定位callback(token, result, lon, lat)
ios.getLocation = function(token,callback){
	try{
		twoant_getLocation(token, callback);
	}catch(e){
		al.tips("调用原生定位异常："+e);
	}
}
//微信登录callback(token, result, code)
ios.loginWX = function(token,state,callback){
	try{
		twoant_loginWX(token,state, callback);
	}catch(e){
		al.tips("调用微信APP登录异常："+e);
	}
}
//微信分享callback(token, result)
ios.shareWX = function(token, type, url, img, title, msg, callback){
	try{
		twoant_shareWX(token, type, url, img, title, msg, callback);
	}catch(e){
		al.tips("调用微信 APP分享异常："+e);
	}
}
//QQ登录callback(token, result, code)
ios.loginQQ = function(token,state,callback){
	try{
		al.tips('ios qq log');
		twoant_loginQQ(token,state, callback);
	}catch(e){
		al.tips("调用QQ APP登录异常："+e);
	}
}
//QQ分享callback(token, result)
ios.shareQQ = function(token, type, url, img, title, msg, callback){
	try{
		twoant_shareQQ(token, type, url, img, title, msg, callback);
	}catch(e){
		al.tips("调用QQ APP分享异常："+e);
	}
}
//扫码callback(token, result, code);
ios.scanCode = function(token, callback){
	try{
		twoant_scanCode(token, callback);
	}catch(e){
		al.tips("调用扫码异常："+e);
	}
}
//支付calback(token, result, order)
ios.pay = function(token, order, tool, callback){
	try{
		twoant_callPay(token, order, tool, callback);
	}catch(e){
		al.tips("调用支付异常："+e);
	}
}
//登录成功后极光推送注册监听
ios.jpushListenerReg = function(token, code, method){
	try{
		twoant_jpushListenerReg(token, code, method);
	}catch(e){
		al.tips("调用推送注册异常："+e);
	}
}
//消息已读
ios.readPush = function(qty){
	try{
		twoant_readPush(qty);
	}catch(e){
		al.tips("调用已读消息异常："+e);
	}
}


var android = {};
//退出应用
android.exit = function(token){
	try{
		window.twoant.twoant_exit(token);
	}catch(e){
		al.tips("退出应用异常："+e);
	}
}
android.copy = function(token,txt){
	try{
		window.twoant.twoant_copy(token,txt);
	}catch(e){
		al.tips("复制异常："+e);
	}
}
//定位callback(token, result, lon, lat)
android.getLocation = function(token,callback){
	try{
		window.twoant.twoant_getLocation(token, callback);
	}catch(e){
		al.tips("调用原生定位异常："+e);
	}
}
//微信登录callback(token, result, code)
android.loginWX = function(token,state,callback){
	try{
		window.twoant.twoant_loginWX(token,state, callback);
	}catch(e){
		al.tips("调用微信APP登录异常："+e);
	}
}

//微信分享callback(token, result)
android.shareWX = function(token, type, url, img, title, msg, callback){
	try{
		window.twoant.twoant_shareWX(token, type, url, img, title, msg, callback);
	}catch(e){
		al.tips("调用微信 APP分享异常："+e);
	}

}
//QQ登录callback(token, result, code)
android.loginQQ = function(token,state,callback){
	try{
		window.twoant.twoant_loginQQ(token,state, callback);
	}catch(e){
		al.tips("调用QQ APP登录异常："+e);
	}
}

//QQ分享callback(token, result)
android.shareQQ = function(token, type, url, img, title, msg, callback){
	try{
		window.twoant.twoant_shareQQ(token, type, url, img, title, msg, callback);
	}catch(e){
		al.tips("调用QQ APP分享异常："+e);
	}
}
//扫码callback(token, result, code);
android.scanCode = function(token, callback){
	try{
		window.twoant.twoant_scanCode(token, callback);
	}catch(e){
		al.tips("调用扫码异常："+e);
	}
}
//支付calback(token, result, order)
android.pay = function(token, order, tool, callback){
	try{
		window.twoant.twoant_callPay(token, order, tool, callback);
	}catch(e){
		al.tips("调用支付异常："+e);
	}
}
//登录成功后注册推送
android.jpushListenerReg = function(token, code, method){
	try{
		window.twoant.twoant_jpushListenerReg(token, code, method);
	}catch(e){
		al.tips("调用推送注册异常："+e);
	}
}
//消息已读
android.readPush = function(qty){
	try{
		window.twoant.twoant_readPush(qty);
	}catch(e){
		//al.tips("调用已读消息异常："+e);
	}
}
</script>
