<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/wap/common/inc/tag.jsp"%>
<div class="btn-group" style="float:right;width:100%;">
<div style="float:right;">
<al:select id="cboSort" data="${sorts }" head="类别"></al:select>
<div class="btn" onclick="fnQuery();" style="width:100px;">查询</div>
<a class="btn" href="a" style="width:100px;">添加</a>
</div>
</div>
<table class="list" style="width:100%;">
	<tr class="list_head">
		<td>序号</td>
		<td>名称</td>
		<td>类别</td>
		<td>价格</td>
		<td>明细</td>
	</tr>
	<tbody id="tbList"></tbody>
</table>
<div id="divNavi"></div>
<script>
function fnQuery(){
	_anyline_navi_conf_0['clear'] = 1;
	_navi_init(_anyline_navi_conf_0);
	_anyline_navi_conf_0['clear'] = 0;
}
function fnGetParam(){
	var params = {'sort':$('#cboSort').val()};
	return params;
}
</script>
<al:navi url="jl"  param="fnGetParam" type="1" naviContainer="divNavi" bodyContainer="tbList"></al:navi>