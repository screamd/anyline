<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/wap/common/inc/tag.jsp"%>
<div class="info" id="divInfo">
	<div class="item">
		<div class="label">标题</div>
		<div class="data">${row.TITLE }</div>
	</div>
	<div class="item">
		<div class="label">副标题</div>
		<div class="data">${row.SUB_TITLE }</div>
	</div>
	<div class="item">
		<div class="label">类别</div>
		<div class="data">${row.SORT_NM }</div>
	</div>
	<div class="item">
		<div class="label">价格</div>
		<div class="data">${row.PRICE }</div>
	</div>
</div>
<a class="btn" href="u?id=${row.ID }">编辑</a>
