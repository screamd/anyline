package org.anyline.demo.json.home.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.anyline.entity.DataRow;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller("json.home.BookController")
@RequestMapping("/js/hm/bk")
public class BookController extends BasicController{
	
	@RequestMapping("js")
	@ResponseBody
	public String item(HttpServletRequest request, HttpServletResponse response){
		DataRow row = service.queryRow("MM_BOOK", parseConfig("+ID:id+"));
		row = entityRow(row, "TITLE:title","SUB_TITLE:subTitle","SORT_ID:sort+", "PRICE:price");
		if(service.exists("MM_BOOK", "TITLE:"+row.getTitle(), "ID:<> "+row.getId())){
			return fail("图书重名");
		}
		DataRow sort = service.queryRow("MM_BOOK_SORT", parseConfig("+id:sort+"));
		if(null == sort){
			return fail("类别不存在");
		}
		row.put("SORT_NM", sort.getNm());
		service.save("MM_BOOK", row);
		return success();
	}

}